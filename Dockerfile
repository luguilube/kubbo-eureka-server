FROM openjdk:11

VOLUME /tmp

EXPOSE 8761

ADD ./target/kubbo-eureka-server-0.0.1-SNAPSHOT.jar kubbo-eureka-server.jar

ENTRYPOINT ["java", "-jar", "/kubbo-eureka-server.jar"]