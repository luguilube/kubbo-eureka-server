## Kubbo Test App

Kubbo Test App es una app basada en una idea inicial de la empresa Kubbo, específicamente como prueba técnica para postulación a una vacante. Esta app fue diseñada en un principio por un conjunto de 6 microservicios para el backend  (servidor, customers, products, warehouses, orders, cloud gateway) para gestionar específicamente lo referente a cada funcionalidad requerida por la App, pero segmentada en módulos; y para el frontend un proyecto únicamente. Actualmente se sigue actualizando que nuevas funcionalidades y tecnologías para ser mostrada como proyecto de portafolio.

## Descripción del Proyecto

Toda la descripción detallada del proyecto, se encuentra dentro del repositorio de [kubbo-test-app-frontend](https://bitbucket.org/luguilube/kubbo-test-app-frontend/src/master/) 

## Kubbo Eureka Server

Microservicio que funciona como un servidor para registrar a todo el resto de microservicios que conforman esta arquitectura, emplea la tecnología Spring Cloud Netflix Eureka Server.

Para comprobar su correcto funcionamiento, una vez habiendo ejecutado todos los microservicios, si se accede a la url local de: localhost:8761. podrá observarse una vista html donde se detallan todos los demás microservicios registrados dentro de este servidor eureka.

## Nota

Este debe ser ser el segundo microservicio en ser ejecutado.
